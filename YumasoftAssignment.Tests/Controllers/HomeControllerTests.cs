﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using YumasoftAssignment;
using YumasoftAssignment.Controllers;
using NUnit.Framework;
using YumasoftAssignment.Services.Interfaces;
using YumasoftAssignment.ViewModels;

namespace YumasoftAssignment.Tests.Controllers
{
    [TestFixture]
    public class HomeControllerTests
    {

        [Test]
        public void Index_WhenModelValid_ReturnsSuccessfullLoginView()
        {
            var controller = new HomeController(new LogonLogService());
            var model = new LoginViewModel();
            
            var result = controller.Index(model) as ViewResult;

            Assert.AreEqual("SuccessfullLogin",result.ViewName);
        }

        [Test]
        public void Index_WhenModelValid_LoginStringIsPassedToView()
        {
            string testLogin= "someLogin";
            var controller = new HomeController(new LogonLogService());
            var model = new LoginViewModel
            {
                Login = testLogin,
                Password = "password"
            };

            var result = controller.Index(model) as ViewResult;

            Assert.AreEqual(testLogin, ((UserDataViewModel)result.Model).Login);
        }

        [Test]
        public void Index_WhenModelValid_ReturnsIndexView()
        {
            var controller = new HomeController(new LogonLogService());
            var model = new LoginViewModel();
            controller.ModelState.AddModelError("fakeError", "fakeError");

            var result = controller.Index(model) as ViewResult;

            Assert.AreEqual("Index", result.ViewName);
        }
                
    }

    public class LogonLogService : ILogonLogService
    {
        public void AddLoginLog(string login)
        {            
        }

        public DateTime? GetLastLoggedTime(string login)
        {
            return DateTime.Now;
        }
    }
}
