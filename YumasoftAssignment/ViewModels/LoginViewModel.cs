﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace YumasoftAssignment.ViewModels
{
    public class LoginViewModel
    {
        [Required]
        [MinLength(5),MaxLength(15)]
        [Display(Name ="Nazwa użytkownika")]
        public string Login { get; set; }
        [Required]
        [MinLength(5), MaxLength(15)]
        [Display(Name = "Hasło")]
        public string Password { get; set; }
    }
}