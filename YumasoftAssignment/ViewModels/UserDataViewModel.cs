﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace YumasoftAssignment.ViewModels
{
    public class UserDataViewModel
    {
        public string Login { get; set; }
        public DateTime? LastLoginTime { get; set; }
    }
}