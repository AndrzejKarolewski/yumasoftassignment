﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using YumasoftAssignment.Services.Interfaces;
using YumasoftAssignment.ViewModels;

namespace YumasoftAssignment.Controllers
{
    public class HomeController : Controller
    {
        private ILogonLogService logonLogService;
        public HomeController(ILogonLogService logonLogService)
        {
            this.logonLogService = logonLogService;
        }

        public ActionResult Index()
        {
            var model = new LoginViewModel();
            return View("Index", model);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Index([ModelBinder(typeof(DevExpress.Web.Mvc.DevExpressEditorsBinder))] LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                var userData = new UserDataViewModel
                {
                    Login = model.Login,
                    LastLoginTime = logonLogService.GetLastLoggedTime(model.Login)
                };

                logonLogService.AddLoginLog(model.Login);

                return View("SuccessfullLogin",userData);
            }

            return View("Index",model);
        }
    }
}