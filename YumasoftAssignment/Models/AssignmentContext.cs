﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using YumasoftAssignment.Models.Interfaces;

namespace YumasoftAssignment.Models
{
    public class AssignmentContext : DbContext, IAssignmentContext
    {
        public AssignmentContext():base()
        {

        }

        public DbSet<LogonLog> LogonLogs { get; set; }
    }
}