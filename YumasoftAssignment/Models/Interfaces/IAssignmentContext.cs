﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YumasoftAssignment.Models.Interfaces
{
    public interface IAssignmentContext
    {
        int SaveChanges();
        DbSet<LogonLog> LogonLogs { get; set; }
    }
}
