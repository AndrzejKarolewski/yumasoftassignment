﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace YumasoftAssignment.Models
{
    public class LogonLog
    {
        public long Id { get; set; }
        [StringLength(15)]
        public string Login { get; set; }
        public DateTime LogTime { get; set; } = DateTime.Now;
    }
}