﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YumasoftAssignment.Services.Interfaces
{
    public interface ILogonLogService
    {
        void AddLoginLog(string login);
        DateTime? GetLastLoggedTime(string login);
    }
}
