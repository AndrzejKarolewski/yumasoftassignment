﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using YumasoftAssignment.Models;
using YumasoftAssignment.Models.Interfaces;
using YumasoftAssignment.Services.Interfaces;

namespace YumasoftAssignment.Services
{
    public class LogonLogService : ILogonLogService
    {
        private IAssignmentContext db;

        public LogonLogService(IAssignmentContext context)
        {
            db = context;
        }

        public void AddLoginLog(string login)
        {
            var log = new LogonLog
            {
                Login = login
            };
            db.LogonLogs.Add(log);
            db.SaveChanges();
        }

        public DateTime? GetLastLoggedTime(string login)
        {
            return db.LogonLogs.OrderByDescending(x => x.Id).FirstOrDefault(x => x.Login == login)?.LogTime;
        }
    }
}